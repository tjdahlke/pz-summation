
#==========================================================================

ER: ${R}/upgoing.pdf ${R}/downgoing.pdf ${R}/inputHydro.pdf ${R}/inputGeo.pdf ${R}/Pwindow.pdf ${R}/Zwindow.pdf


NR:


CR:

#==========================================================================

${R}/upgoing.v: up2D.H.binned
	Cp $< $@1
	echo "label1='Time[s]' label2='Source X position [m]' " >> $@1
	Grey gainpanel=a pclip=95 titles=" " < $@1 ${FIG}$@
	rm $@1

${R}/downgoing.v: down2D.H.binned
	Cp $< $@1
	echo "label1='Time[s]' label2='Source X position [m]' " >> $@1
	Grey gainpanel=a pclip=95 titles=" " < $@1 ${FIG}$@
	rm $@1

${R}/inputHydro.v: hydro_line2D.H.BP.binned
	Cp $< $@1
	echo "label1='Time[s]' label2='Source X position [m]' " >> $@1
	Grey pclip=95 titles=" " < $@1 ${FIG}$@
	rm $@1

${R}/inputGeo.v: vert_line2D.H.BP.binned
	Cp $< $@1
	echo "label1='Time[s]' label2='Source X position [m]' " >> $@1
	Grey pclip=95 titles=" " < $@1 ${FIG}$@
	rm $@1

${R}/Pwindow.v: Pwindow.h.binned
	Cp $< $@1
	echo "label1='Time[s]' label2='Source X position [m]' " >> $@1
	Grey pclip=100 titles=" " < $@1 ${FIG}$@
	rm $@1

${R}/Zwindow.v: Zwindow.h.binned
	Cp $< $@1
	echo "label1='Time[s]' label2='Source X position [m]' " >> $@1
	Grey pclip=100 titles=" " < $@1 ${FIG}$@
	rm $@1

#==========================================================================
%.pdf: %.v
	pstexpen $< $@1 color=y fat=1 fatmult=1.5 invras=y
	epstopdf $@1
	rm $@1 


