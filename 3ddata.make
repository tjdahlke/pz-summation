

#-------------------------------------------------------
# RAW+DESIGNTURED NODE GATHERS
${D}/SingleNode%.H:
	Window_key synch=1 key1=nodeid maxk1=$* mink1=$* < ${fullHYRD} > $@ hff=$@@@ datapath=${SCR}

${D}/SingleNode%.V:
	Window_key synch=1 key1=nodeid maxk1=$* mink1=$* < ${fullVERT} > $@ hff=$@@@ datapath=${SCR}


offset%.H: ${D}/SingleNode%.H
	Window3d n1=1 f1=9 < $(shell Get parform=n < $< hff ) > $@

#-------------------------------------------------------
# DO CROSS-CORRELATIONS
filternode=3
zauto${filternode}.H Pwindow${filternode}.h Zwindow${filternode}.h: ${B}/Corr.x ${D}/SingleNode${filternode}.V  ${D}/SingleNode${filternode}.H
	${B}/Corr.x zcorr=zauto${filternode}.H pzcross=pzcross${filternode}.H vert=${D}/SingleNode${filternode}.V hydr=${D}/SingleNode${filternode}.H \
	par=${P}/picking${filternode}.p Zwindow=Zwindow${filternode}.h  Pwindow=Pwindow${filternode}.h >/dev/null

#-------------------------------------------------------
# MAKE THE FILTER
filter${filternode}.H: ${B}/Filter_PZ.x zauto${filternode}.H
	${B}/Filter_PZ.x  par=${P}/filter.p zcorr=zauto${filternode}.H pzcross=pzcross${filternode}.H > $@
	./headcp.sh $@ zauto${filternode}.H

#-------------------------------------------------------
# APPLY FILTER
${project}/UP/up%.H ${project}/DOWN/down%.H: ${B}/Pzsum.x filter${filternode}.H ${D}/SingleNode%.V ${D}/SingleNode%.H
	${B}/Pzsum.x slowness=${project}/slowness$*.h vert=${D}/SingleNode$*.V hydr=${D}/SingleNode$*.H \
	filter=filter${filternode}.H water_vel=1475. upgoing=${project}/UP/up$*.H downgoing=${project}/DOWN/down$*.H > /dev/null

nodeID_header.H@@: ${fullHYRD}
	Headermath maxsize=100000 \
	key1=nodeId eqn1='nodeid' \
	delete_keys=${tosskeys} \
	< $< >/dev/null hff=$@;

rec.p: nodeID_header.H@@
	python ${PY}/findAllNodeIDs.py infile=$< outfile=$@

#-------------------------------------------------------
# RUN PZ-SUMMATION ON CLUSTER
PBS-pz-summation: rec.p
	python ${PY}/PBS-pz-summation.py rec.p ${pwd}/wrk/ ${pwd} ${PY}/PBStemplate.sh


#-------------------------------------------------------
# AFTERWARDS, MAKE A SINGLE FILE FOR THE UP AND DOWN
UP_final.H:
	python ${PY}/recursive_cat.py 10 $@ 'UP/up*.H'
	echo "hff=${pwd}/$@@@" >> $@

DOWN_final.H:
	python ${PY}/recursive_cat.py 10 $@ 'DOWN/down*.H'
	echo "hff=${pwd}/$@@@" >> $@



