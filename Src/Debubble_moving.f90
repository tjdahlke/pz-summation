program Debubble_moving
  ! Program for removing bubble from data
  ! Based on GEE's pef and helicon modules
  ! Calculates moving window containing reflections
  !   PEF is built based on those reflections
  !
  ! Inputs:
  !   1) history file of data
  !   2) nw: length of wavelet window
  !   3) t0:  zero-offset estimate (s)
  !   4) niter: number of iterations for pef
  !   5) nlag: length of filter
  !   6) ngap: length of gap
  !   7) offset: history file of offsets (optional: If you have gridded data
  !           where the n2 axis is offset, then that will work fine as an input
  !           and the program will ignore the offset=offsetfile.H type input argument.
  !   8) vel: water velocity
  !   9) maxoff: maximum offset to consider in windowing for PEF
  !   10) training: (optional) If you want to train the PEF data on a different set of traces

  ! Outputs:
  !   1) history file of de-bubbled data
  !   2) window: optional output of data window
  !
  ! Taylor Dahlke, March 2018
  ! Bob Clapp, March 2016
  !
  ! VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
  !     NOTE: USE IFORT COMPILE FLAG: -heap-arrays SO THAT THE RESHAPE FUNCTION DOESNT FAIL FOR LARGE ARRAYS
  !     NOTE2: If the PEF training data contains too much data, hconest will crash for some reason. 
  !           Use less data in this case (lower the maxoff maybe?)
  ! VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV

  !=================================
  ! standard declarations
  !=================================
  use sep
  use pef
  use heliconPEF
  implicit none


  !=================================
  ! declare variables
  !=================================
  ! program variables
  integer                        :: ii,stat,it,ix,k,count
  integer                        :: nlag,niter,ngap
  real                           :: tt,vel,t0,dx,ox,maxoff

  ! input traces
  integer                        :: n1,n2,n123,n1o,n2w, n2t
  real                           :: o1,d1
  real,dimension(:,:),pointer    :: in2d,inw
  real, dimension(:),pointer     :: input,output,training,out1d
  real, dimension(:),allocatable :: offset

  ! wavelet variables
  integer                        :: nw,fw
  type(filter)                   :: aa

  !=================================
  ! open I/O
  !=================================
  call sep_init()

  ! history file parameters
  call from_history('n1',n1,1)
  call from_history('n2',n2,1)
  call from_history('o1',o1,0.)
  call from_history('d1',d1,1.)


  ! wavelet window parameters
  call from_param('nw',nw,100)
  call from_param('t0',t0,0.)
  call from_param('vel',vel,1500.)
  call from_param('maxoff',maxoff,4000000000.)
  call from_param("niter",niter,100)
  call from_param("nlag",nlag,25)
  call from_param("ngap",ngap,50)


  call to_history('n1',n1)
  call to_history('o1',o1)
  call to_history('d1',d1)
  call to_history('n2',n2)

  ! Number of training traces
  if (exist_file('training')) then
    call from_aux("training","n2",n2t)
  else
    n2t=n2
  endif
  write(0,*) "n2t= ", n2t
  write(0,*) "n2= ", n2

  ! optional window output
  if (exist_file('window')) then
    call to_history('n1',n1,'window')
    call to_history('o1',o1,'window')
    call to_history('d1',d1,'window')
    call to_history('n2',n2t,'window')
  end if


  !=================================
  ! Initialization
  !=================================
  ! set variables
  n123=n1*n2t

  allocate(training(n1))
  call allocatehelix(aa,nlag);
  allocate(aa%mis(n123))
  allocate(offset(n2t))

  !=================================
  ! kernel
  !=================================

  ! Either read in the offset info 
  ! (if data not on regular grid)
  ! Or define it from the regular gridding
  if (exist_file('offset')) then
    write(0,*) "Reading offset file (second axis of input is non-regular)"
    call sep_read(offset,'offset')
    count=0
    do ix=1,n2t
      if (offset(ix)<maxoff) then
        count=count+1
      end if
    end do 
    n2w=count
  else
    write(0,*) "Calculating offsets (second axis of input is regularized)"
    call from_history('d2',dx)
    call from_history('o1',ox)
    do ix=1,n2
      offset(ix)=ox+(ix-1)*dx
      if (offset(ix)<maxoff) then
        n2w=ix
      end if
    end do    
  end if


  write(0,*) "Populate lags for filter"
  ! essentially a gap deconvolution?
  do ii=1,nlag ! Starts at lag=1, not lag=0! Thats because we know for a PEF the zero-lag value = 1.0!
    aa%lag(ii)=ii+ngap
  end do


  write(0,*) "Grab windowed portions"
  write(0,*) "n2w =", n2w
  allocate(inw(nw,n2w))
  allocate(out1d(n1))
  out1d=0.0
  write(0,*)">>> maxval(offset) =", maxval(offset)
  write(0,*)">>> minval(offset) =", minval(offset)
  if (minval(offset)>maxoff) then
    write(0,*)">>> All of your input traces have an offset that is bigger than your maxoff parameter. Make maxoff bigger."
    stop
  end if


  count=1
  do ii=1,n2t
    write(0,*)" percent read = ", 100.*ii/n2t
    out1d=0.0
    call sep_read(training,'training')
    if (offset(ii)<=maxoff) then ! Ignore the large offsets if maxoff set
      tt = sqrt( (t0*t0) + ((offset(ii)*offset(ii))/(vel*vel)) ) ! Window according to a hyperbola
      it = 1.5+(tt-o1)/d1
      out1d(it:it+nw) = training(it:it+nw)
      inw(:,count) = training(it:it+nw)
      count=count+1
    end if
    call srite('window',out1d,4*n1)
  end do
  deallocate(training)


  write(0,*) "Build PEF"
  allocate(input(size(inw)))
  input = RESHAPE(inw,(/size(inw)/))
  deallocate(inw)
  write(0,*) "Starting PEF build"
  call find_pef(input,aa,niter)
  deallocate(input)


  write(0,*) "Apply PEF"
  allocate(input(n1),output(n1))
  call helicon_init(aa)
  do ix=1,n2
    write(0,*)" percent written = ", 100.*ix/n2
    call sep_read(input)
    stat = helicon_lop(.false.,.false.,input,output)
    call sep_write(output)
  end do
  write(0,*) " DONE!"

  call sep_close()

end program




