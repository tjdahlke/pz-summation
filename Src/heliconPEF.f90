# 1 "<stdin>"
# 1 "<built-in>"
# 1 "<command-line>"
# 1 "<stdin>"
module heliconPEF
! Convolution, inverse to deconvolution.
  use adj_mod
! Requires the filter be causal with an implicit "1." at the onset.
  use helix
  implicit none
  type( filter), private :: aa
  contains
  subroutine helicon_init ( aa_in )
    type( filter) :: aa_in
    aa = aa_in
  end subroutine
  function helicon_lop ( adj, add, xx, yy) result(stat)
    integer :: stat
    logical,intent(in) :: adj,add
    real,dimension(:) :: xx,yy
    call adjnull (adj,add,xx,yy )
    call helicon_lop2(adj,add,xx,yy )
    stat=0
  end function
  subroutine helicon_lop2(adj,add,xx,yy)
    logical,intent(in) :: adj,add
    real, dimension (:) :: xx
    real, dimension (:) :: yy
    integer iy, ix, ia
    
    ! zero lag (basically xx = yy for all elements, with no aa coefficient)
    ! For PEF, have this uncommented (forces the first filter element to be 1.0)
    ! For regular filter, COMMENT this section
    if ( adj) then
      xx = xx + yy
    else
      yy = yy + xx
    end if

    ! All other filter lags
    do ia = 1, size( aa%lag)
      do iy = 1 + aa%lag( ia), size( yy)
        if ( associated( aa%mis)) then
          if ( aa%mis( iy)) then
            cycle
          end if
        end if
        ix = iy - aa%lag( ia)
        if ( adj) then
          xx(ix) = xx(ix) + yy(iy) * aa%flt(ia)
        else
          yy(iy) = yy(iy) + xx(ix) * aa%flt(ia)
        end if
      end do
    end do
  end subroutine
  subroutine helicon_close()
  end subroutine
end module
