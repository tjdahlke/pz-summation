
module slope

  contains
  subroutine slow(slowness,vert_slow_refrac,offset,wat_vel,tmax,d1,n1,depth)
    ! Routine that gives the vertical slowness for each time sample 
    ! This considers a constant velocity model and uses the velocity picked during the filter design for the refracted arrival
     
    ! Arguments of the routine
    real, dimension(:)    :: slowness
    real                  :: vert_slow_refrac, offset, wat_vel, tmax, d1, depth, k2
    integer               :: n1 

    ! Internal variables
    real(kind=8)          :: tmin_hyp, ray_par, time
    integer               :: tmin_hyp_ix, i

    ! tmin_hyp = (offset + depth) / wat_vel ! In this way I consider all the area under the direct arrival to be hyperbolic with the water velocity (time domain) t0 not = 0. 
    tmin_hyp = SQRT(offset**2 + depth**2) / wat_vel ! In this way I consider all the area under the direct arrival to be hyperbolic with the water velocity (time domain) t0 not = 0. 
    tmin_hyp_ix = nint(tmin_hyp / d1 + 1)
    ! Set the local slope for refracted arrivals and the reflections with water velocity (NMO equation not HMO)
    if (tmin_hyp_ix <= n1) then
      slowness(1:tmin_hyp_ix-1) = 1. / vert_slow_refrac ! Inverse of the vertical slowness
      time = tmin_hyp  ! NOTE the direct wave from source to receiver is not centered at t0=0.  
      do i = tmin_hyp_ix, n1
        ray_par = offset / (time * wat_vel**2.)
        time = time + d1
        ! I'm discriminating using k2 because otherwise introduces NaNs in slowness
        k2 = wat_vel**-2. - ray_par**2.
        if(k2 > 0.0) then
          slowness(i) = 1. / sqrt(k2)
        else
          slowness(i) = 0.
        end if
      enddo
    elseif (tmin_hyp_ix > n1) then ! For traces at far enough offsets that the direct arrival doesnt exist in the data because its recorded later in time, so we assume all refractions in the trace.
      slowness(:) = 1. / vert_slow_refrac ! Inverse of the vertical slowness
    end if
    slowness(:) = slowness(:) * vert_slow_refrac ! Renormalize because in the filter the slowness factor is already inside it.

  block
  integer :: i
  do i=1,size(slowness)
    if(isnan(slowness(i))) then
      write(0,*) "NaNs in slowness!"
      write(0,*) "vert_slow_refrac: ",vert_slow_refrac
    end if
    if(slowness(i) == 0.) then
      write(0,*) "slowness is zero, i, nslowness: ",i,size(slowness)
    end if
  end do
  end block

  end subroutine slow

end module slope











! module slope

! contains
! subroutine slow(slowness,vert_slow_refrac,offset,wat_vel,tmax,d1,n1,depth)
!   ! Routine that gives the vertical slowness for each time sample 
!   ! This considers a constant velocity model and uses the velocity picked during the filter design for the refracted arrival
   
!   ! Arguments of the routine
!   real, dimension(:)		:: slowness
!   real				          :: vert_slow_refrac, offset, wat_vel, tmax, d1, depth
!   integer			          :: n1 

!   ! Internal variables
!   real(kind=8)          :: tmin_hyp, ray_par, time
!   integer			          :: tmin_hyp_ix, i

!   tmin_hyp = SQRT(offset**2 + depth**2) / wat_vel -0.05 ! In this way I consider all the area under the direct arrival to be hyperbolic with the water velocity (time domain) t0 not = 0. 
!   tmin_hyp_ix = nint(tmin_hyp / d1 + 1)
!   ! Set the local slope for refracted arrivals and the reflections with water velocity (NMO equation not HMO)
!   if (tmin_hyp_ix <= n1) then
!     slowness(1:tmin_hyp_ix-1) = 1. / vert_slow_refrac ! Inverse of the vertical slowness
!     time = tmin_hyp  ! NOTE the direct wave from source to receiver is not centered at t0=0.  
!     do i = tmin_hyp_ix, n1
!       ray_par = offset / (time * wat_vel**2.)
!       time = time + d1
!       slowness(i) = 1. / sqrt(wat_vel**-2. - ray_par**2.)
!     enddo
!   elseif (tmin_hyp_ix > n1) then ! For traces at far enough offsets that the direct arrival doesnt exist in the data because its recorded later in time, so we assume all refractions in the trace.
!     slowness(:) = 1. / vert_slow_refrac ! Inverse of the vertical slowness
!   end if
!   slowness(:) = slowness(:) * vert_slow_refrac ! Renormalize because in the filter the slowness factor is already inside it.


! end subroutine slow

! end module slope
