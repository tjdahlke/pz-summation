!!$=head1 NAME
!!$
!!$Filter_PZ - Program that computes the Wiener filter needed for the PZ summation
!!$
!!$=head1 SYNOPSIS
!!$
!!$Filter_PZ  zcorr= pzcross= filter_length= epsi=0.1 > filter.H
!!$
!!$=head1 DESCRIPTION
!!$
!!$ Compute the Wiener shaping filter for the PZ summation considering the z axes pointing upward
!!$ so that U(f) = 1/2*P(f) + filt(f)*rho/(2*q)*Z(f)
!!$
!!$=head1 PARAMETERS
!!$
!!$=over 3
!!$
!!$=item from history 
!!$
!!$=item from par
!!$
!!$B<filename> I<zcorr> - Name of the file containing the Autocorrelation of the vertical component
!!$
!!$B<filename> I<pzcross> - Name of the file containing the Crosscorrelation of the Pressure with the vertical component
!!$
!!$B<real> I<filter_length> - Length of the Shaping filter in seconds
!!$
!!$B<real> I<epsi> - Percentage of the white noise for the prewithening (default value 0.1%)
!!$
!!$
!!$=back
!!$
!!$=head1 EXAMPLES
!!$
!!$=head1 MODULES
!!$
!!$=head1 SEE ALSO
!!$
!!$=cut
!%


program Filter_PZ

  use sep
  use Wiener_filters

  implicit none
  integer                                 :: n1
  real                                    :: d1, epsi
  character*256                           :: zcorrfile, pzcrossfile
  real                                    :: filter_length !shaping filter length
  integer                                 :: n_filter !number of samples of the filter
  real, allocatable, dimension(:)         :: zauto, pzcross, filter
  integer                                 :: zero_ix, i
  integer, dimension(3)                   :: header

  ! Call initializer for SEP routines
  call sep_init (SOURCE)
  call init_3d ()
  ! Retrieve the information from parameter and aux files 
  call from_par ("filter_length",filter_length)
  call from_par ("zcorr",zcorrfile)
  call from_par ("pzcross",pzcrossfile)
  call from_par ("epsi",epsi,0.1)

  call from_aux("zcorr","n1",n1)
  call from_aux("zcorr","d1",d1)
  
  ! Initialize internal variables
  epsi = epsi / 100.
  n_filter = nint(filter_length/d1)+1 
  allocate(zauto(n1),pzcross(n1))
  allocate(filter(n_filter))
  filter(:) = 0.

  ! Reading the data
  call sep_read(zauto,"zcorr")
  call sep_read(pzcross,"pzcross")

  ! Write header information to the output filter
  call to_history("n1",n_filter)
  call to_history("o1",0.)
  call to_history("d1",d1)
  call to_history("d2",1.)
  call to_history("label1","Time s")

  ! Call the shaping deconvolution
  zero_ix = (n1-1)/2 + 1

  ! Reverse order of pzbuff because we need zpcross and take the negative part
  call Shaping(zauto(zero_ix:1:-1),pzcross(zero_ix:1:-1),filter(:),epsi)

  !Write the filter coefficients
  call sep_write(filter)

  deallocate(zauto,pzcross,filter)
  call sep_close()
  call exit (0)
end program Filter_PZ
