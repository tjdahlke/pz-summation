! Module to read the picking table for PZ and create a related object
! Pick file must be sorted by common receiver location (gx,gy), common source position (sx,sy), picked time (t)
MODULE pick_obj
  USE ieee_arithmetic
  IMPLICIT NONE
  TYPE pick_entry
     REAL (kind=8), DIMENSION(2) :: t
     REAL (kind=8) :: gx,gy
     REAL (kind=8), DIMENSION(2) :: sx,sy
  END TYPE pick_entry
  TYPE pick
     TYPE(pick_entry), POINTER :: pick_list(:)
     ! TYPE(pick_entry), POINTER, PRIVATE :: pick_list(:)
     REAL (kind=8), DIMENSION(:,:), ALLOCATABLE, PRIVATE :: unit_vector ! component of the unit vector of the picked receivers
     REAL (kind=8)     :: gxchk, gychk
     REAL(kind=8)	    :: tntrp(2), sxntrp(2), syntrp(2), usx, usy
   CONTAINS
      PROCEDURE, pass(pick_arg) :: decode_pick_file
      PROCEDURE, pass(pick_arg) :: deallocate_pick
      PROCEDURE, pass(pick_arg) :: writing_values
      PROCEDURE, pass(pick_arg) :: window
      PROCEDURE, pass(pick_arg) :: vector
      ! PROCEDURE, pass(pick_arg) :: return_gx
      ! PROCEDURE, pass(pick_arg) :: return_gy
      ! PROCEDURE, pass(pick_arg) :: return_sx
      ! PROCEDURE, pass(pick_arg) :: return_sy
      ! PROCEDURE, pass(pick_arg) :: return_size
  END TYPE pick




CONTAINS



  ! FUNCTION return_size(pick_arg)
  !   IMPLICIT NONE
  !   ! Input variables
  !   class(pick), INTENT(inout)      :: pick_arg
  !   INTEGER                         :: return_size
  !   return_size = SIZE(pick_arg%pick_list)
  !   RETURN
  ! END FUNCTION return_size



  ! FUNCTION return_sx(pick_arg,pkid)
  !   IMPLICIT NONE
  !   ! Input variables
  !   class(pick), INTENT(inout)      :: pick_arg
  !   INTEGER,INTENT(in)              :: pkid
  !   REAL (kind=8), DIMENSION(2)     :: return_sx
  !   return_sx = pick_arg%pick_list(pkid)%sx
  !   RETURN
  ! END FUNCTION return_sx


  ! FUNCTION return_sy(pick_arg,pkid)
  !   IMPLICIT NONE
  !   ! Input variables
  !   class(pick), INTENT(inout)      :: pick_arg
  !   INTEGER,INTENT(in)              :: pkid
  !   REAL (kind=8), DIMENSION(2)     :: return_sy
  !   return_sy = pick_arg%pick_list(pkid)%sy
  !   RETURN
  ! END FUNCTION return_sy



  ! FUNCTION return_gx(pick_arg,pkid)
  !   IMPLICIT NONE
  !   ! Input variables
  !   class(pick), INTENT(inout)      :: pick_arg
  !   INTEGER,INTENT(in)              :: pkid
  !   REAL                            :: return_gx
  !   return_gx = pick_arg%pick_list(pkid)%gx
  !   RETURN
  ! END FUNCTION return_gx



  ! FUNCTION return_gy(pick_arg,pkid)
  !   IMPLICIT NONE
  !   ! Input variables
  !   class(pick), INTENT(inout)      :: pick_arg
  !   INTEGER,INTENT(in)              :: pkid
  !   REAL                            :: return_gy
  !   return_gy = pick_arg%pick_list(pkid)%gy
  !   RETURN
  ! END FUNCTION return_gy



  SUBROUTINE decode_pick_file (pick_arg, filename)
    IMPLICIT NONE
    CHARACTER*(*), INTENT(in)  :: filename
    class(pick), INTENT(inout) :: pick_arg

    CHARACTER*256 :: line_buffer
    INTEGER :: open_stat,read_stat
    INTEGER :: line_count, rec_count
    LOGICAL :: first
    TYPE(pick_entry) :: pick, previous_pick
    !Open the file to be read and count the number of receivers
    OPEN(file=filename,unit=22,form='formatted', status='old', iostat=open_stat)
    IF (open_stat .NE. 0 ) THEN
       WRITE(0,*) "Cannot read the input file"
       RETURN
    ENDIF

    read_stat=0
    line_count=0
    rec_count=0
    first=.TRUE.
    DO WHILE (read_stat == 0)
       READ(unit=22,fmt='(A)',iostat=read_stat) line_buffer
       IF (read_stat == 0) THEN
          IF (line_buffer(1:1)=='#') CYCLE
          line_count = line_count + 1
          ! READ(line_buffer,*) pick%t(1), pick%sx(1), pick%sy(1), pick%gx, pick%gy !ORIGINAL
          READ(line_buffer,*) pick%t(1), pick%sy(1), pick%sx(1), pick%gx, pick%gy 
          IF (first) THEN
             previous_pick=pick
             first=.FALSE.
             rec_count= rec_count + 1
          ENDIF
          IF (pick%gx .NE. previous_pick%gx .OR. pick%gy .NE. previous_pick%gy) THEN
             rec_count= rec_count + 1
          ENDIF
          previous_pick=pick
       ENDIF
    ENDDO

    ! Counts the receiver number for allocation. So needs receivers gy and gx to be sorted (either descending or ascending)
    ! so that when reciever values changes we know we have a new receivers to count.
    REWIND(unit=22)
    IF (rec_count > 0) THEN
       ALLOCATE(pick_arg%pick_list(1:rec_count))
       read_stat=0
       first=.TRUE.
       rec_count=0
       DO WHILE (read_stat == 0)
          READ(unit=22,fmt='(A)',iostat=read_stat) line_buffer
          IF (read_stat == 0) THEN
             IF (line_buffer(1:1)=='#') CYCLE ! Specific to the picking parameter file
             ! READ(line_buffer,*) pick%t(1), pick%sx(1), pick%sy(1), pick%gx, pick%gy !ORIGINAL
              READ(line_buffer,*) pick%t(1), pick%sy(1), pick%sx(1), pick%gx, pick%gy
             IF (first) THEN
                previous_pick=pick
                first=.FALSE.
                rec_count= rec_count + 1
                pick_arg%pick_list(rec_count)=pick
             ENDIF
             IF (pick%gx .NE. previous_pick%gx .OR. pick%gy .NE. previous_pick%gy) THEN
                rec_count= rec_count + 1
                pick_arg%pick_list(rec_count)=pick
             ELSE
                pick_arg%pick_list(rec_count)%sx(2)=pick%sx(1)
                pick_arg%pick_list(rec_count)%sy(2)=pick%sy(1)
                pick_arg%pick_list(rec_count)%t(2)=pick%t(1)
             ENDIF
             previous_pick=pick
          ENDIF
       ENDDO
    ENDIF

    CLOSE(unit=22)
    RETURN
  END SUBROUTINE decode_pick_file




  SUBROUTINE deallocate_pick(pick_arg)
    class(pick), INTENT(inout) :: pick_arg
    DEALLOCATE(pick_arg%pick_list)
    RETURN
  END SUBROUTINE deallocate_pick



  SUBROUTINE writing_values(pick_arg)
    IMPLICIT NONE
    class(pick), INTENT(in) :: pick_arg
    INTEGER :: i
    DO i = 1, SIZE(pick_arg%pick_list)
       WRITE(0,*) "Picking values rec pos. Gx = ", pick_arg%pick_list(i)%gx, " Gy = ", pick_arg%pick_list(i)%gy
       WRITE(0,*) "Time 1 = ", pick_arg%pick_list(i)%t(1), "Time 2 =", pick_arg%pick_list(i)%t(2)
       WRITE(0,*) "Sx 1 = ", pick_arg%pick_list(i)%sx(1), "Sx 2 =", pick_arg%pick_list(i)%sx(2)
       WRITE(0,*) "Sy 1 = ", pick_arg%pick_list(i)%sy(1), "Sy 2 =", pick_arg%pick_list(i)%sy(2)
    ENDDO
    WRITE(0,*) ""
    RETURN
  END SUBROUTINE writing_values




  SUBROUTINE vector(pick_arg)
    class(pick), INTENT(inout)   :: pick_arg
    REAL(kind=8)			:: dist, x, y
    INTEGER   			:: pkid

    ALLOCATE(pick_arg%unit_vector(SIZE(pick_arg%pick_list)-1,2))
    DO pkid = 1, SIZE(pick_arg%pick_list)-1
       x = pick_arg%pick_list(pkid+1)%gx - pick_arg%pick_list(pkid)%gx
       y = pick_arg%pick_list(pkid+1)%gy - pick_arg%pick_list(pkid)%gy
       dist = SQRT((x)**2. + (y)**2.)
       pick_arg%unit_vector(pkid,1)= x / dist
       pick_arg%unit_vector(pkid,2)= y / dist
    ENDDO
    RETURN
  END SUBROUTINE vector





  SUBROUTINE vector_source(sx,sy,usx,usy)
    REAL(kind=8)			:: dist, x, y
    INTEGER   			:: pkid
    REAL (kind=8), INTENT(in)	:: sx(2),sy(2)
    REAL (kind=8), INTENT(out)	:: usx,usy

    x = sx(2)- sx(1)
    y = sy(2)- sy(1)
    dist = SQRT((x)**2. + (y)**2.)
    usx = x / dist
    usy = y / dist
    RETURN
  END SUBROUTINE vector_source




  SUBROUTINE lin_inter(inter,dot1,dot2,y1,y2)
    REAL(kind=8) :: inter,dot1,dot2,y1,y2,sumdot
    sumdot =  1. / (dot1 + dot2)
    inter = sumdot *(dot2 * y1 + dot1 * y2)
    RETURN
  END SUBROUTINE lin_inter





  FUNCTION window(pick_arg,gx,gy,sx,sy,t)
    IMPLICIT NONE
    !Input variables
    class(pick), INTENT(inout)    :: pick_arg
    INTEGER, INTENT(in)           :: gx, gy, sx, sy
    !Output variables
    LOGICAL		                    :: window
    REAL(kind=8)	                :: t
    !Internal variables
    INTEGER		                    :: pkid
    REAL(kind=8)	                :: dot1, dot2


    ! Checking in which picked receiver interval the input trace falls using unit vectors
    DO pkid = 1, SIZE(pick_arg%pick_list)-1
       ! Calculate the dot product
       dot1=pick_arg%unit_vector(pkid,1)*(gx-pick_arg%pick_list(pkid)%gx)+ &
            pick_arg%unit_vector(pkid,2)*(gy-pick_arg%pick_list(pkid)%gy)
       dot2=-pick_arg%unit_vector(pkid,1)*(gx-pick_arg%pick_list(pkid+1)%gx)- &
            pick_arg%unit_vector(pkid,2)*(gy-pick_arg%pick_list(pkid+1)%gy)

! write(0,*) "gx = ", gx
! write(0,*) "pick_arg%pick_list(pkid)%gx = ", pick_arg%pick_list(pkid)%gx

       IF(dot1 >= 0. .AND. dot2 >= 0) THEN
          ! The receiver is between two picked receivers
          ! Linear interpolation of time, sx, sy and computation of the source interpolated pick unit vectors

! write(0,*) "The receiver is between two picked receivers 1"
! write(0,*) "pick_arg%gxchk =", pick_arg%gxchk
! write(0,*) "pick_arg%gychk =", pick_arg%gychk
! write(0,*) "gy =", gy
! write(0,*) "gx =", gx

          IF (pick_arg%gxchk .NE. gx .OR. pick_arg%gychk .NE. gy) THEN
! write(0,*) "The receiver is between two picked receivers 2"
             CALL lin_inter(pick_arg%tntrp(1),dot1,dot2,pick_arg%pick_list(pkid)%t(1),pick_arg%pick_list(pkid+1)%t(1))
             CALL lin_inter(pick_arg%tntrp(2),dot1,dot2,pick_arg%pick_list(pkid)%t(2),pick_arg%pick_list(pkid+1)%t(2))
             CALL lin_inter(pick_arg%sxntrp(1),dot1,dot2,pick_arg%pick_list(pkid)%sx(1),pick_arg%pick_list(pkid+1)%sx(1))
             CALL lin_inter(pick_arg%sxntrp(2),dot1,dot2,pick_arg%pick_list(pkid)%sx(2),pick_arg%pick_list(pkid+1)%sx(2))
             CALL lin_inter(pick_arg%syntrp(1),dot1,dot2,pick_arg%pick_list(pkid)%sy(1),pick_arg%pick_list(pkid+1)%sy(1))
             CALL lin_inter(pick_arg%syntrp(2),dot1,dot2,pick_arg%pick_list(pkid)%sy(2),pick_arg%pick_list(pkid+1)%sy(2))
             CALL vector_source(pick_arg%sxntrp,pick_arg%syntrp,pick_arg%usx,pick_arg%usy)
             pick_arg%gxchk = gx
             pick_arg%gychk = gy
          ENDIF
          EXIT
       ELSEIF (dot1 < 0. .AND. pkid == 1) THEN
! write(0,*) "The receiver x coordinate is smaller of the left-end receiver 1"

! write(0,*) "pick_arg%gxchk =", pick_arg%gxchk
! write(0,*) "pick_arg%gychk =", pick_arg%gychk
! write(0,*) "gy =", gy
! write(0,*) "gx =", gx

          !The receiver x coordinate is smaller of the left-end receiver
          IF (pick_arg%gxchk .NE. gx .OR. pick_arg%gychk .NE. gy) THEN
! write(0,*) "The receiver x coordinate is smaller of the left-end receiver 2"
             CALL lin_inter(pick_arg%tntrp(1),dot1,dot2,pick_arg%pick_list(pkid)%t(1),pick_arg%pick_list(pkid+1)%t(1))
             CALL lin_inter(pick_arg%tntrp(2),dot1,dot2,pick_arg%pick_list(pkid)%t(2),pick_arg%pick_list(pkid+1)%t(2))
             CALL lin_inter(pick_arg%sxntrp(1),dot1,dot2,pick_arg%pick_list(pkid)%sx(1),pick_arg%pick_list(pkid+1)%sx(1))
             CALL lin_inter(pick_arg%sxntrp(2),dot1,dot2,pick_arg%pick_list(pkid)%sx(2),pick_arg%pick_list(pkid+1)%sx(2))
             CALL lin_inter(pick_arg%syntrp(1),dot1,dot2,pick_arg%pick_list(pkid)%sy(1),pick_arg%pick_list(pkid+1)%sy(1))
             CALL lin_inter(pick_arg%syntrp(2),dot1,dot2,pick_arg%pick_list(pkid)%sy(2),pick_arg%pick_list(pkid+1)%sy(2))
             CALL vector_source(pick_arg%sxntrp,pick_arg%syntrp,pick_arg%usx,pick_arg%usy)
             pick_arg%gxchk = gx
             pick_arg%gychk = gy
          ENDIF
          EXIT
       ELSEIF (dot2 < 0. .AND. (pkid+1) == SIZE(pick_arg%pick_list)) THEN
! write(0,*) "The receiver x coordinate is greater than right-end receiver"
          !The receiver x coordinate is greater than right-end receiver
          IF (pick_arg%gxchk .NE. gx .OR. pick_arg%gychk .NE. gy) THEN
             CALL lin_inter(pick_arg%tntrp(1),dot1,dot2,pick_arg%pick_list(pkid)%t(1),pick_arg%pick_list(pkid+1)%t(1))
             CALL lin_inter(pick_arg%tntrp(2),dot1,dot2,pick_arg%pick_list(pkid)%t(2),pick_arg%pick_list(pkid+1)%t(2))
             CALL lin_inter(pick_arg%sxntrp(1),dot1,dot2,pick_arg%pick_list(pkid)%sx(1),pick_arg%pick_list(pkid+1)%sx(1))
             CALL lin_inter(pick_arg%sxntrp(2),dot1,dot2,pick_arg%pick_list(pkid)%sx(2),pick_arg%pick_list(pkid+1)%sx(2))
             CALL lin_inter(pick_arg%syntrp(1),dot1,dot2,pick_arg%pick_list(pkid)%sy(1),pick_arg%pick_list(pkid+1)%sy(1))
             CALL lin_inter(pick_arg%syntrp(2),dot1,dot2,pick_arg%pick_list(pkid)%sy(2),pick_arg%pick_list(pkid+1)%sy(2))
             CALL vector_source(pick_arg%sxntrp,pick_arg%syntrp,pick_arg%usx,pick_arg%usy)
             pick_arg%gxchk = gx
             pick_arg%gychk = gy
          ENDIF
          EXIT
       ENDIF
    ENDDO
    CALL pick_interp(pick_arg%tntrp,pick_arg%sxntrp,pick_arg%syntrp,pick_arg%usx,pick_arg%usy,sx,sy,t)
    IF (t == -HUGE(t)) THEN
       window = .FALSE.
    ELSE
       window = .TRUE.
    ENDIF
    RETURN
  END FUNCTION window




  SUBROUTINE pick_interp(tntrp,sxntrp,syntrp,usx,usy,sx,sy,t)
    !Linear intepolation
    IMPLICIT NONE
    REAL(kind=8),INTENT(in)     :: tntrp(2), sxntrp(2), syntrp(2), usx, usy
    REAL(kind=8)                :: t
    INTEGER, INTENT(in)         :: sx, sy
    REAL(kind=8)                :: dot1, dot2, sumdot
    !Computed the dot products of the picked sources with the current source
    dot1 = usx * (sx - sxntrp(1)) + usy * (sy - syntrp(1))
    dot2 = -usx * (sx - sxntrp(2)) - usy * (sy - syntrp(2))
! write(0,*) "============================================="
! write(0,*) "sx =", sx
! write(0,*) "sxntrp =", sxntrp
! write(0,*) "sy =", sy
! write(0,*) "syntrp =", syntrp
    IF (dot1 >= 0. .AND. dot2 >= 0) THEN
       !The source of the trace considered is inside the window
       !Interpolate to find the time for the considered trace
       sumdot = 1. / (dot1 + dot2)
       t = sumdot * ( tntrp(1) * dot2 + tntrp(2) * dot1 )
    ELSE
       !The source considered is outside the window
       t=-HUGE(t)
    ENDIF
    RETURN
  END SUBROUTINE pick_interp


END MODULE pick_obj
