!!$=head1 NAME
!!$
!!$Cross - Calculate the correlations needed for designing shaping filter
!!$
!!$=head1 SYNOPSIS
!!$
!!$PZ < in.H corr_length= zcorr= pzcross= tL1= tR1= offL= offR= > /dev/null
!!$
!!$=head1 DESCRIPTION
!!$
!!$ Compute the autocorrelations needed for designing the shaping filter for the PZ summation using the method of PZ
!!$ calibration by applying the equation of motion to critically refracted waves" Melbo et al. (2002)
!!$
!!$=head1 PARAMETERS
!!$
!!$=item from history
!!$
!!$=item from par
!!$
!!$B<real> I<corr_length> - Length of the autocorrelations and crosscorrelation files (one side seconds)
!!$
!!$B<filename> I<zcorr> - Name of the file containing the Autocorrelation of the vertical component
!!$
!!$B<filename> I<pzcross> - Name of the file containing the Crosscorrelation of the Pressure with the vertical component
!!$
!!$B<float> I<tL1> - Time at top left corner of desired window on node gather sorted by absolute valued offset
!!$
!!$B<float> I<tR1> - Time at top right corner of desired window on node gather sorted by absolute valued offset
!!$
!!$B<float> I<offL> - Offset marking left side of desired window on node gather sorted by absolute valued offset
!!$
!!$B<float> I<offR> - Offset marking right side of desired window on node gather sorted by absolute valued offset
!!$
!!$=back
!!$
!!$=cut
!%


PROGRAM Corr

  USE sep
  USE ieee_arithmetic
  USE correlation

  IMPLICIT NONE
  INTEGER                                 :: nt, traceindex, iheader, ntraces, numsamples
  INTEGER, DIMENSION(:),ALLOCATABLE       :: header
  REAL                                    :: gx, gy, ot, dt, offL, offR, tL1, tR1,tTOP,tBOT
  INTEGER                                 :: TOP, BOTTOM
  REAL, DIMENSION(:,:), ALLOCATABLE       :: component_traces
  REAL, DIMENSION(:), ALLOCATABLE         :: tracewindowP, tracewindowZ
  REAL           :: corr_length, o1_corr
  INTEGER				 :: ncorr !number of samples per trace of the correlation files
  INTEGER				 :: top_ix, bottom_ix
  REAL, DIMENSION(:,:), ALLOCATABLE	      :: corr_buffer ! buffer for the autocorrelations of P and Z, and crosscorrelation of PZ
  INTEGER				 :: PAUTO,ZAUTO,PZCROSS,PCOMP,XCOMP,YCOMP,ZCOMP
  INTEGER				 :: sreed, rc, itrace
  INTEGER				 :: srite, wc
  INTEGER				 :: hc, hwc, sep_put_key, sep_put_number_keys, sep_put_val_headers, sep_put_header_axis_par, sep_copy_header_keys, sep_copy_hff
  REAL					 :: offset,hor_slow
  INTEGER				 :: hdrvals(3)
  INTEGER        :: GXHD,GYHD,SLPHD
  PARAMETER(TOP=1,BOTTOM=2)
  PARAMETER(ZAUTO=1,PZCROSS=2)
  PARAMETER(PCOMP=1,ZCOMP=2)
  PARAMETER(GXHD=1,GYHD=2,SLPHD=3)

  ! Initialize
  CALL sep_init (SOURCE)
  CALL init_3d ()

  ! Read in parameters
  CALL from_aux("hydr","n1",nt)
  CALL from_aux("hydr","d1",dt)
  CALL from_aux("hydr","o1",ot)
  CALL from_aux("hydr","n2",ntraces)
  CALL from_par ("corr_length",corr_length)
  CALL from_par ("offL",offL)
  CALL from_par ("offR",offR)
  CALL from_par ("tL1",tL1)
  CALL from_par ("tR1",tR1)

  ! Allocate stuff
  ALLOCATE(component_traces(nt,2))
  ALLOCATE(tracewindowP(nt))
  ALLOCATE(tracewindowZ(nt))

  ! Initialize internal variables
  ! ncorr = NINT(corr_length/dt)*2-1 !(Starting and ending with 1 element overlap)
  numsamples=NINT(corr_length/dt)
  ncorr = numsamples*2-1 !(Starting and ending with 1 element overlap)
  write(0,*) "corr_length = ",corr_length
  write(0,*) "dt = ",dt
  write(0,*) "numsamples = ",numsamples
  write(0,*) "ncorr = ",ncorr
  ALLOCATE(corr_buffer(ncorr,2))
  corr_buffer(:,:) = 0
  o1_corr = -corr_length+dt

  ! Writing header information for the various outputs
  CALL to_history("n1",ncorr,"zcorr")
  CALL to_history("n1",ncorr,"pzcross")
  CALL to_history("n1",nt,"Pwindow")
  CALL to_history("n1",nt,"Zwindow")
  CALL to_history("d1",dt,"zcorr")
  CALL to_history("d1",dt,"pzcross")
  CALL to_history("d1",dt,"Pwindow")
  CALL to_history("d1",dt,"Zwindow")
  CALL to_history("o1",o1_corr,"zcorr")
  CALL to_history("o1",o1_corr,"pzcross")
  CALL to_history("o1",ot,"Pwindow")
  CALL to_history("o1",ot,"Zwindow")

  write(0,*) "ntraces =", ntraces
  CALL to_history("n2",ntraces,"Pwindow")
  CALL to_history("n2",ntraces,"Zwindow")

  ! Create output header entries
  hc = sep_put_number_keys("zcorr",3)
  hc = sep_put_number_keys("pzcross",3)
  hc = sep_put_key("zcorr","gx","scalar_int",'xdr_int',GXHD)
  hc = sep_put_key("pzcross","gx","scalar_int",'xdr_int',GXHD)
  hc = sep_put_key("zcorr","gy","scalar_int",'xdr_int',GYHD)
  hc = sep_put_key("pzcross","gy","scalar_int",'xdr_int',GYHD)
  hc = sep_put_key("zcorr","apparent_velocity","scalar_int",'xdr_int',SLPHD)
  hc = sep_put_key("pzcross","apparent_velocity","scalar_int",'xdr_int',SLPHD)

  ! Copy the header keys from input to window display output
  hc = sep_copy_header_keys("hydr", "Pwindow")
  hc = sep_copy_header_keys("hydr", "Zwindow")
  hc = sep_copy_hff("hydr", "Pwindow")
  hc = sep_copy_hff("hydr", "Zwindow")
  hc = sep_put_header_axis_par("Pwindow",2,ntraces,1.,1.,"trace number")
  hc = sep_put_header_axis_par("Zwindow",2,ntraces,1.,1.,"trace number")

  ! Gets the node info from the first trace (assumes all input traces are from the same node!)
  traceindex=1
  call sep_get_val_by_name("hydr",traceindex,"rx",1,gx)
  call sep_get_val_by_name("hydr",traceindex,"ry",1,gy)

  ! Horizontal slowness  
  hor_slow = ABS((offL - offR)/(tL1 - tR1))

  ! Initialize all the variables
  iheader=1
  rc=1

!VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
  DO WHILE (rc > 0) ! Keep reading data till there is none
      ! write(0,*)"percent done: ", 100*iheader/ntraces
      tracewindowP=0.0
      tracewindowZ=0.0

      ! Trying to separate out the different components (read for trace at each time)
      rc=sreed("hydr" , component_traces(:,PCOMP) , nt*4)
      rc=sreed("vert" , component_traces(:,ZCOMP) , nt*4)
      ! Exit loop after hitting the last trace
      IF(rc < 1) EXIT

      ! Get offset information about trace
      call sep_get_val_by_name("hydr",iheader,"offset",1,offset)
      iheader=iheader+1

      ! Check if trace is in the offset window
      IF ((offL < offset) .and. (offset < offR)) THEN

          ! Calculate the top of window time
          tTOP=tL1+(offset - offL)/hor_slow

          ! Find the time window of the trace to cross-correlate
          top_ix = NINT(tTOP/dt)
          bottom_ix=top_ix + numsamples

          ! Do the cross-correlations
          CALL correl(component_traces(:,ZCOMP), component_traces(:,ZCOMP), corr_buffer(:, ZAUTO), top_ix,bottom_ix) !compute autoc of Z
          CALL correl(component_traces(:,PCOMP), component_traces(:,ZCOMP), corr_buffer(:, PZCROSS), top_ix,bottom_ix) !compute cross of PZ

          ! Output the window of the Pressure component data used for the upgoing filter
          tracewindowP(top_ix:top_ix+numsamples) = tracewindowP(top_ix:top_ix+numsamples) + component_traces(top_ix:top_ix+numsamples,PCOMP)
          tracewindowZ(top_ix:top_ix+numsamples) = tracewindowZ(top_ix:top_ix+numsamples) + component_traces(top_ix:top_ix+numsamples,ZCOMP)

      ENDIF

      ! Write the window output
      wc=srite("Pwindow",tracewindowP,nt*4)
      wc=srite("Zwindow",tracewindowZ,nt*4)

  ENDDO
!VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV

  ! Write the correlations to disk
  wc=srite("zcorr",corr_buffer(:, ZAUTO),ncorr*4)
  wc=srite("pzcross",corr_buffer(:, PZCROSS),ncorr*4)

  ! Write the headers values
  hdrvals(1) = gx
  hdrvals(2) = gy
  hdrvals(3) = hor_slow
  hwc = sep_put_val_headers ("zcorr",1,1,hdrvals)
  hwc = sep_put_val_headers ("pzcross",1,1,hdrvals)

  ! End the program
  CALL sep_close()
  CALL EXIT (0)
  WRITE(0,*)"DONE!!"



END PROGRAM Corr


