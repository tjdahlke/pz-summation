module fourier

  implicit none
  include "fftw3.f"

  complex, dimension(:), allocatable, private  :: in1, out1, in2, out2
  integer, private                             :: gbl_n1, gbl_n2
  integer*8, private                           :: plan1f, plan1b, plan2f, plan2b
  contains

  subroutine ft_init(n1_in, n2_in)
    integer              :: n1_in, n2_in
    gbl_n1 = n1_in
    gbl_n2 = n2_in  
    allocate(in1(gbl_n1), out1(gbl_n1))
    allocate(in2(gbl_n2), out2(gbl_n2))
    in1 = 0.
    out1 = 0.
    in2 = 0.
    out2 = 0.
    call sfftw_plan_dft_1d(plan1f, gbl_n1, in1, out1, FFTW_FORWARD, FFTW_ESTIMATE)
    call sfftw_plan_dft_1d(plan1b, gbl_n1, out1, in1, FFTW_BACKWARD, FFTW_ESTIMATE)
    call sfftw_plan_dft_1d(plan2f, gbl_n2, in2, out2, FFTW_FORWARD, FFTW_ESTIMATE)
    call sfftw_plan_dft_1d(plan2b, gbl_n2, out2, in2, FFTW_BACKWARD, FFTW_ESTIMATE)
  end subroutine
   
  subroutine ft_destroy
    deallocate(in1, out1)
    deallocate(in2, out2)
  end subroutine

  subroutine ft1axis(adj, sign1, cdata)
  ! Forward and Ajoint/Inverse Fourier Transform on axis 1
    logical, intent(in)        :: adj
    integer                    :: i2
    complex, dimension(:,:)    :: cdata
    real                       :: sign1
    if(adj) then
      do i2=1,gbl_n2
        !out1(1:(gbl_n1+1)/2) = cdata(gbl_n1-(gbl_n1+1)/2+1:gbl_n1,i2)
        !out1((gbl_n1+1)/2+1:gbl_n1) = cdata(1:gbl_n1-(gbl_n1+1)/2,i2)
        out1 = cdata(:,i2)
        call sfftw_execute_dft(plan1b, out1, in1)
        cdata(:,i2) = in1
      enddo
    else
      do i2=1,gbl_n2
        in1 = cdata(:,i2)
        call sfftw_execute_dft(plan1f, in1, out1)
        cdata(:,i2) = out1
        !cdata(1:gbl_n1-(gbl_n1+1)/2,i2) = out1((gbl_n1+1)/2+1:gbl_n1)
        !cdata(gbl_n1-(gbl_n1+1)/2+1:gbl_n1,i2) = out1(1:(gbl_n1+1)/2)
      enddo
    endif
    cdata = sign1*cdata/sqrt(1.*gbl_n1)
  end subroutine

  subroutine ft2axis(adj, sign2, cdata)
  ! Forward and Ajoint/Inverse Fourier Transform on axis 2
    logical, intent(in)        :: adj
    integer                    :: i1
    complex, dimension(:,:)    :: cdata
    real                       :: sign2
    if(adj) then

      do i1=1,gbl_n1
        out2 = cdata(i1,:)
        call sfftw_execute_dft(plan2f, out2, in2)
	cdata(i1,:) = in2
        !cdata(i1,:) = sign2*out2/sqrt(1.*gbl_n2)
        !cdata(i1,1:gbl_n2-(gbl_n2+1)/2) = out2((gbl_n2+1)/2+1:gbl_n2)
        !cdata(i1,gbl_n2-(gbl_n2+1)/2+1:gbl_n2) = out2(1:(gbl_n2+1)/2)
      enddo
    cdata = sign2*cdata/sqrt(1.*gbl_n2)

    else
      do i1=1,gbl_n1
        in2 = cdata(i1,:)
        call sfftw_execute_dft(plan2f, in2, out2)
        cdata(i1,:) = out2!cdata(i1,:) = sign2*out2/sqrt(1.*gbl_n2)
        !cdata(i1,1:gbl_n2-(gbl_n2+1)/2) = out2((gbl_n2+1)/2+1:gbl_n2)
        !cdata(i1,gbl_n2-(gbl_n2+1)/2+1:gbl_n2) = out2(1:(gbl_n2+1)/2)
      enddo
    endif
    cdata = sign2*cdata/sqrt(1.*gbl_n2)
  end subroutine

end module
