!Module for the Wiener filters design using then Levinson recursion
!Note that in Shaping is assumed that the matrix is simmetric (i.e. forward = reverse(backward))
MODULE Wiener_filters
CONTAINS
  SUBROUTINE Shaping(zauto,zpcross,filter,epsi)
    !Arguments of the subroutine
    REAL, DIMENSION(:)		:: zauto,zpcross,filter
    REAL				:: epsi
    !Internal variables
    REAL, DIMENSION(:),ALLOCATABLE	:: forward_vector
    REAL				:: err, alfa, err_filt
    INTEGER				:: i, j, k
    REAL :: debug

    ALLOCATE(forward_vector(SIZE(zauto)))
    forward_vector(:) = 0.

    !Add the white noise to the main diagonal
    zauto(1) = (1 + epsi) * zauto(1)

    !Start the inversion
    !First iteration solves 1x1 problem
    filter(1) = zpcross(1) / zauto(1)
    forward_vector(1) = 1 / zauto(1)
    DO i = 2, SIZE(filter)
       err = 0.
       err_filt = 0.
       DO j = 2, i
          err = err + zauto(i-j+2) * forward_vector(j-1)
       ENDDO
       alfa = 1. / (1. - err**2.)
       forward_vector(1:i) = alfa * forward_vector(1:i) - err * alfa * forward_vector(i:1:-1)
       DO k = 2, i
          err_filt = err_filt + zauto(i-k+2) * filter(k-1)
       ENDDO
       filter(1:i) = filter(1:i) + (zpcross(i) - err_filt) * forward_vector(i:1:-1)
    ENDDO



    DEALLOCATE(forward_vector)
  END SUBROUTINE Shaping
END MODULE Wiener_filters
