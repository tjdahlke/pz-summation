
module correlation

 contains
!Subroutine to compute the correlation of the input traces trace1*trace2
!The correlation is computed within a window that takes in account that outside there are non-zero samples in the correlated traces
  subroutine correl(trace1, trace2, corr_wind, top_ix, bottom_ix)
     !Arguments
     real, dimension(:), intent(in) 	:: trace1, trace2
     real, dimension(:), intent(inout)	:: corr_wind
     integer, intent(in)		:: top_ix, bottom_ix
     !Internal variables
     integer 				:: i, lag, zero_lag_ix, length_window

     length_window = bottom_ix - top_ix + 1
     zero_lag_ix = (size(corr_wind)-1)/2 + 1
     do i = 1, size(corr_wind)
        lag = i - zero_lag_ix
        corr_wind(i) = corr_wind(i) + dot_product(trace1(top_ix:bottom_ix), trace2(top_ix + lag: bottom_ix + lag))
     enddo

     return
  end subroutine correl
end module correlation
