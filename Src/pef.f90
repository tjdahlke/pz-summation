# 1 "<stdin>"
# 1 "<built-in>"
# 1 "<command-line>"
# 1 "<stdin>"
module pef
! Find prediction-error filter (helix magic)
  use hconest
  use cgstep_mod
  use solver_smp_mod
  implicit none
  contains
  subroutine find_pef( dd, aa, niter)
    integer, intent( in) :: niter
    ! number of iterations
    type( filter) :: aa ! filter
    real, dimension(:), pointer :: dd ! input data
    call hconest_init( dd, aa)
    write(0,*) "Starting solver"
    call solver_smp(m=aa%flt, d=-dd, Fop=hconest_lop, stepper=cgstep,&
      & niter=niter, m0=aa%flt,verb=.true.)
    call cgstep_close()
  end subroutine
end module
