!!$=head1 NAME
!!$
!!$Pzsum - Program that perfoms the PZ summation
!!$
!!$=head1 SYNOPSIS
!!$
!!$Pzsum < in.H filter= water_vel=1500. upgoing= downgoing= > /dev/null
!!$
!!$=head1 DESCRIPTION
!!$
!!$ Perfom the wavefield separation considering a costant velocity model using the water velocity
!!$ given by the user in the command line. The shaping filter (i.e. calibration filter)
!!$ Note that the filter and the input file must have the same temporal sampling and
!!$ it is necessary to give the program all the components (P,x,y,z)
!!$
!!$=head1 PARAMETERS
!!$
!!$=over 3
!!$
!!$=item from history
!!$
!!$=item from par
!!$
!!$B<filename> I<filter> - Name of the file containing the calibration filter for each receiver.
!!$
!!$B<real> I<water_vel> - Water velocity (default value 1500 [m/s])
!!$
!!$B<filename> I<upgoing> - Name of the output file containing upgoing wavefield
!!$
!!$B<filename> I<downgoing> - Name of the output file containing downgoing wavefield
!!$
!!$=back
!!$
!!$=head1 EXAMPLES
!!$
!!$=head1 MODULES
!!$
!!$=head1 SEE ALSO
!!$
!!$=cut
!%

program Pzsum

    use sep
    use fourier
    use slope

    implicit none
    integer :: nt, n2, n1f, n2f, ii, ntraces
    real    :: dt, ot, wat_vel, sign1, vert_slow_refrac, offset, tmax, gwdep, sx, sy, gx, gy, maxoffset
    real, allocatable, dimension(:) :: filter
    integer, dimension(:), allocatable :: header_f
    integer :: PCOMP, XCOMP, YCOMP, ZCOMP
    integer :: GXHD, GYHD, VELHD ! Index for the header of the filter (it has only three components)
    integer :: iheader
    integer :: ptrace_ix, ztrace_ix, itrace
    integer :: sreed, srite
    integer :: rc, wc
    real, dimension(:,:),allocatable :: component_traces
    integer :: rec_id, maxlen
    complex, dimension(:,:), allocatable :: ctrace, cfilter
    real, dimension(:),allocatable :: upgoing, downgoing, slowness
    integer :: hc, sep_copy_header_keys,sep_put_header_axis_par, sep_put_val_headers, sep_put_number_keys, sep_copy_hff
    integer :: ih, sep_put_key, sep_get_key_name, sep_get_key_type, sep_get_key_fmt
    character*120 :: keyname, keytype, keyformat

    parameter(PCOMP=1,XCOMP=3,YCOMP=4,ZCOMP=2)
    parameter(GXHD=1,GYHD=2,VELHD=3)

    call sep_init()
    call init_3d ()

    ! Read the data information
    call from_aux("hydr","n1",nt)
    call from_aux("hydr","d1",dt) !Same temporal sampling of the filter
    call from_aux("hydr","o1",ot)
    call from_aux("hydr","n2",ntraces)

    ! Read filter information
    call from_aux("filter","n1",n1f)
    call from_par ("water_vel",wat_vel,1500.)

    ! Allocate memory for the calibration filter and reading it from the parameter file
    allocate(filter(n1f))
    call sep_read(filter,"filter")

    ! Allocate variables
    allocate(component_traces(nt,2))
    allocate(header_f(3))
    maxlen = max(nt, n1f)
    allocate(ctrace(maxlen,1), cfilter(maxlen,1))
    allocate(upgoing(maxlen), downgoing(maxlen), slowness(maxlen))

    ! Allocate space for the FFT
    call ft_init(maxlen,1)

    ! Initialize the variables
    sign1 = 1.
    ctrace = 0.
    cfilter = 0.
    slowness = 0.
    tmax = ot + (maxlen - 1)*dt
    iheader=0
    rc=1
    ii=1

    ! Put the dimensions of the output files
    call to_history("n1",maxlen,"upgoing")
    call to_history("d1",dt,"upgoing")
    call to_history("o1",ot,"upgoing")
    call to_history("n2",ntraces,"upgoing")
    call to_history("n1",maxlen,"downgoing")
    call to_history("d1",dt,"downgoing")
    call to_history("o1",ot,"downgoing")
    call to_history("n2",ntraces,"downgoing")
    call to_history("n1",maxlen,"slowness")
    call to_history("d1",dt,"slowness")
    call to_history("o1",ot,"slowness")
    call to_history("n2",ntraces,"slowness")

    ! Copy the header keys from input to output
    hc = sep_copy_header_keys("hydr", "upgoing")
    hc = sep_copy_header_keys("hydr", "downgoing")
    hc = sep_copy_header_keys("hydr", "slowness")
    hc = sep_copy_hff("hydr", "upgoing")
    hc = sep_copy_hff("hydr", "downgoing")
    hc = sep_copy_hff("hydr", "slowness")
    hc = sep_put_header_axis_par("upgoing",2,ntraces,1.,1.,"trace number")
    hc = sep_put_header_axis_par("downgoing",2,ntraces,1.,1.,"trace number")
    hc = sep_put_header_axis_par("slowness",2,ntraces,1.,1.,"trace number")


!VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
    do while (rc > 0)

        ! write(0,*)"percent done: ", 100*ii/ntraces
        ii=ii+1
        iheader = iheader+1

        ! Read for traces at each time
        rc=sreed("hydr" , component_traces(:,1) , nt*4)
        rc=sreed("vert" , component_traces(:,2) , nt*4)
        ! Stop after reading the last trace
        if(rc < 1) exit

        call sep_get_val_by_name("hydr",iheader,"gwdep",1,gwdep)
        call sep_get_val_by_name("hydr",iheader,"rx",1,gx)
        call sep_get_val_by_name("hydr",iheader,"ry",1,gy)
        call sep_get_val_by_name("hydr",iheader,"sx",1,sx)
        call sep_get_val_by_name("hydr",iheader,"sy",1,sy)

        ptrace_ix=1
        ztrace_ix=2

        ! Find to which receiver gather the trace belongs
        rec_id=1
        call sep_get_val_headers("filter", rec_id, 1, header_f)
        vert_slow_refrac = sqrt(wat_vel**-2. - real(header_f(VELHD)**-2.)) !vertical slowness of the refracted arrivals
        cfilter = 0.
        cfilter(1:n1f,1) = filter(:)
        call ft1axis(.false., sign1, cfilter)
        ctrace(1:nt,1) = component_traces(:,ztrace_ix)

        ! Trasform the Z-component trace and convolve with the filter
        call ft1axis(.false., sign1, ctrace)
        ctrace(:,1) = cfilter(:,1) * ctrace(:,1) * sqrt(maxlen * 1.) !Get rid of the normalization factor in excess
        call ft1axis(.true., sign1, ctrace)

        ! Computation of the absoffset and vertical slowness
        offset = abs(sqrt(real(gx-sx)**2.+real(gy-sy)**2.)) ! Assumes the the units are in meters
        call slow(slowness,vert_slow_refrac,offset,wat_vel,tmax,dt,maxlen,gwdep)
        ! write(0,*)"vert_slow_refrac =", vert_slow_refrac
        ! write(0,*)"wat_vel =", wat_vel
        ! write(0,*)"tmax =", tmax
        ! write(0,*)"maxlen =", maxlen
        ! write(0,*)"gwdep =", gwdep
        ! write(0,*)"offset =", offset
        ! write(0,*)"maxval(slowness) =", MAXVAL(slowness)
        ! write(0,*)"minval(slowness) =", MINVAL(slowness)

        ! Perfom the PZ summation for the wavefield separation
        upgoing = (component_traces(:,ptrace_ix) + slowness(:)*real(ctrace(:,1)))/2.
        downgoing = (component_traces(:,ptrace_ix) - slowness(:)*real(ctrace(:,1)))/2.

        ! Write the output files and the header of corresponding trace
        wc=srite("upgoing",upgoing,nt*4)
        wc=srite("downgoing",downgoing,nt*4)
        wc=srite("slowness",slowness,nt*4)

        ctrace = 0.
        slowness = 0.
    enddo
!VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV

    call sep_close()
    call exit (0)

end program Pzsum
