
####################################################################################################################
# PZ summation Cardamom Dataset
#
# Created by Taylor Dahlke
# Starting date: 06/23/2018
# Makefile for perfoming the up-going and down-going wavefields using the PZ summation 
# on the Cardamom OBN dataset
####################################################################################################################
include makefile.top

###############################################################################################################
# BASIC MAKERULES
###############################################################################################################

default: EXE
	make setdatapaths

EXE: ${B}/Corr.x ${B}/Filter_PZ.x ${B}/Pzsum.x

setdatapaths:
	echo "datapath=${pwd}/scratch/" > .datapath
	
# binpar=key1='offset' ng1=250 dg1=40 og1=0.0
binpar=key1='sx' ng1=200 dg1=50 og1=209030 compress_tr=0
%.binned: %
	Sort3d nkeys=1 ${binpar} < $< > $@1 hff=$@1@@ gff=$@1gff
	Stack3d normalize=y < $@1 > $@
	rm $@1 $@1@@ $@1gff


###############################################################################################################
# Default make rules 
###############################################################################################################

#clean all figures, and also clean the executables and the intermediate result file (.H).
burn: jclean
	rm -f ${B}/*
	rm -f ${O}/*
	rm -f ${D}/*
	rm -f ${M}/*
	rm -f ./*.H*
	rm -f ./*.h*
	rm -rf scratch/*
	rm -f *.F*
	rm -f core.*
	rm -f .make.dependencies.LINUX
	rm -f ${R}/*
	

###############################################################################################################
# Make thesis / sep-174 figures
include 2dline.make
include Figures.make
include 3ddata.make









