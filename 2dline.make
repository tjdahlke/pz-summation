
#-------------------------------------------------------
# GRAB THE 2D LINES FOR EACH COMPONENT

filternode=3
keys= nkeys=2 key1=nodeid mink1=${filternode} maxk1=${filternode} key2=nominalshotline mink2=1486 maxk2=1486
hydro_line2D.H:
	Window_key synch=1 ${keys} < ${fullHYRD} > $@1
	Window3d max1=6.0 < $@1 > $@
	rm $@1

vert_line2D.H: 
	Window_key synch=1 ${keys} < ${fullVERT} > $@1
	Window3d max1=6.0 < $@1 > $@
	rm $@1

%.BP: %
	Bandpass flo=1.0 < $< > $@

#-------------------------------------------------------
# DO BANDPASS

offset2d.H: hydro_line2D.H.BP
	Window3d n1=1 f1=0 < $(shell Get parform=n < $< hff ) > $@

#-------------------------------------------------------
# GRAB SINGLE NODE GATHER FOR FILTER
hydro_line2D.H.BP.F: hydro_line2D.H.BP
	Window_key synch=1 key1=nodeid maxk1=${filternode} mink1=${filternode} < $< > $@

vert_line2D.H.BP.F: vert_line2D.H.BP
	Window_key synch=1 key1=nodeid maxk1=${filternode} mink1=${filternode} < $< > $@

#-------------------------------------------------------
# DO CROSS-CORRELATIONS
zauto.H pzcross.H Pwindow.h Zwindow.h: ${B}/Corr.x vert_line2D.H.BP.F  hydro_line2D.H.BP.F
	${B}/Corr.x vert=vert_line2D.H.BP.F hydr=hydro_line2D.H.BP.F \
	Zwindow=Zwindow.h Pwindow=Pwindow.h \
	zcorr=zauto.H pzcorr=pzcross.H par=${P}/picking2Dline.p >/dev/null

#-------------------------------------------------------
# MAKE THE FILTER
filter2D.H: ${B}/Filter_PZ.x zauto.H pzcross.H
	${B}/Filter_PZ.x par=${P}/filter.p zcorr=zauto.H pzcross=pzcross.H > $@
	./headcp.sh $@ zauto.H

#-------------------------------------------------------
# APPLY FILTER
up2D.H down2D.H slowness2D.h: ${B}/Pzsum.x filter2D.H vert_line2D.H.BP hydro_line2D.H.BP
	${B}/Pzsum.x slowness=slowness2D.h vert=vert_line2D.H.BP hydr=hydro_line2D.H.BP \
	filter=filter2D.H water_vel=1475. upgoing=up2D.H downgoing=down2D.H > /dev/null





