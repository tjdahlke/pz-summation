

# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
#
#   PROGRAM GRABS NODE GATHERS AND DOES PROCESSING ON THEM
#
# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX


#!/usr/local/bin/python
import sys
import os
import math
import commands
import sepbase
import sep2npy
import numpy as np
import subprocess


# Read the inputs
if len(sys.argv) < 3:
    print('Error: Not enough input args')
    sys.exit()
recfile = sys.argv[1]
writepath = sys.argv[2]
runpath = sys.argv[3]
pbsTemplate = sys.argv[4]

print("\n\n")
print(recfile)
cmd1="less %s | wc -l" % (recfile)
stat1,numNodes=commands.getstatusoutput(cmd1)
recList = list(open(recfile))



# Visit each location to grab the binned shot data
for ii in range(0,int(numNodes)):

    print("----------------------------------------")
    percent=100.0*ii/float(numNodes)
    percentcmd="percent done:  %f " % (percent)
    print(percentcmd)

    nodeId=int(str(recList[ii]).split()[0])
    outputFileName="%s/DOWN/down%d.H" % (runpath,nodeId)
    temp2="%s/temp2_%d.sh" % (writepath,nodeId)
    finalPBS="%s/PBSscript_%d.sh" % (writepath,nodeId)


    # Check if file already exists
    check="ls %s" % (outputFileName)
    stat2,junk=commands.getstatusoutput(check)
    if (stat2>0):

        cmdp3="echo '#PBS -N pbsjob-%s' >> %s" % (nodeId,temp2)
        cmdp4="echo '#PBS -d %s' >> %s" % (writepath,temp2)
        cmdp5="echo 'setenv DATAPATH %s/' >> %s" % (writepath,temp2)
        cmdp="%s; %s; %s;" % (cmdp3, cmdp4, cmdp5)
        print(cmdp)
        subprocess.call(cmdp,shell=True)

        # Run the job for a single node
        cmd="make %s;" % (outputFileName)
        # cmd="pwd"
        cmdW1="echo '%s' >> %s;" % (cmd,temp2)
        print(cmdW1)
        subprocess.call(cmdW1,shell=True)

        cmdW3="cat %s %s > %s;" % (pbsTemplate,temp2,finalPBS)
        print(cmdW3)
        subprocess.call(cmdW3,shell=True)
        cdmS="sleep 1"
        print(cdmS)
        subprocess.call(cdmS,shell=True)
        cmdW4="qsub -l nodes=1:ppn=24 -o %s -d %s %s;" % (writepath,runpath,finalPBS)
        print(cmdW4)
        subprocess.call(cmdW4,shell=True)





